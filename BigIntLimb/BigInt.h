#include <vector>
#include <iostream>
#include <string>
#include <algorithm>

struct BigInt
{
	std::vector<unsigned int> limbs;
	unsigned int base = 1u; //se [unit�] << 1u -> base 2^1 -> base 2
	bool sign;

	BigInt();
	BigInt(long long unsigned, bool = false);
	BigInt(const std::vector<unsigned int> ls, bool = false);

	BigInt operator+(const BigInt& b) const;
	BigInt operator-(const BigInt& b) const;
	BigInt operator~() const;
	BigInt operator&(const BigInt& b) const;
	BigInt operator|(const BigInt& b) const;
	bool operator<(const BigInt& b) const;
	bool operator>(const BigInt& b) const;
	bool operator>=(const BigInt& b) const;
	bool operator<=(const BigInt& b) const;
	bool operator==(const BigInt& b) const;
	BigInt operator-() const;
	size_t NumLimbs() const { return limbs.size(); }
	friend std::ostream& operator<<(std::ostream& out, const BigInt& c);
};