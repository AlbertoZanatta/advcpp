// BigIntLimb.cpp : Questo file contiene la funzione 'main', in cui inizia e termina l'esecuzione del programma.
//

#include <iostream>
#include "BigInt.h"
#include <chrono>
#include <vector>
using std::vector;

vector<long long> naive_mul(const vector<long long>& x, const vector<long long>& y) {
    auto len = x.size();
    vector<long long> res(2 * len);

    for (auto i = 0; i < len; ++i) {
        for (auto j = 0; j < len; ++j) {
            res[i + j] += x[i] * y[j];
        }
    }

    return res;
}

vector<long long> karatsuba_mul(const vector<long long>& x, const vector<long long>& y) {
    auto len = x.size();
    vector<long long> res(2 * len);

    if (len <= 1) {
        return naive_mul(x, y);
    }

    auto k = len / 2;

    vector<long long> Xr{ x.begin(), x.begin() + k };
    vector<long long> Xl{ x.begin() + k, x.end() };
    vector<long long> Yr{ y.begin(), y.begin() + k };
    vector<long long> Yl{ y.begin() + k, y.end() };

    vector<long long> P1 = karatsuba_mul(Xl, Yl);
    vector<long long> P2 = karatsuba_mul(Xr, Yr);

    vector<long long> Xlr(k);
    vector<long long> Ylr(k);

    for (auto i = 0; i < k; ++i) {
        Xlr[i] = Xl[i] + Xr[i];
        Ylr[i] = Yl[i] + Yr[i];
    }

    vector<long long> P3 = karatsuba_mul(Xlr, Ylr);

    for (auto i = 0; i < len; ++i) {
        P3[i] -= P2[i] + P1[i];
    }

    for (auto i = 0; i < len; ++i) {
        res[i] = P2[i];
    }

    for (auto i = len; i < 2 * len; ++i) {
        res[i] = P1[i - len];
    }

    for (auto i = k; i < len + k; ++i) {
        res[i] += P3[i - k];
    }

    return res;
}

void BigIntTest()
{
    //std::cout << (1u << 1);
    BigInt a(4);
    BigInt b(2);

    auto t1 = std::chrono::high_resolution_clock::now();
    //std::cout << (BigInt::power(u, 256)) << std::endl;
    BigInt res = b - a;
    auto t2 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
    std::cout << duration << std::endl;
}

void KaratsubaTest()
{
    vector<long long> a{ 4,3,2,1 };
    vector<long long> b{ 8,7,6,5 };
    vector<long long> res = karatsuba_mul(a, b);
}
int main()
{
    KaratsubaTest();
}

// Per eseguire il programma: CTRL+F5 oppure Debug > Avvia senza eseguire debug
// Per eseguire il debug del programma: F5 oppure Debug > Avvia debug

// Suggerimenti per iniziare: 
//   1. Usare la finestra Esplora soluzioni per aggiungere/gestire i file
//   2. Usare la finestra Team Explorer per connettersi al controllo del codice sorgente
//   3. Usare la finestra di output per visualizzare l'output di compilazione e altri messaggi
//   4. Usare la finestra Elenco errori per visualizzare gli errori
//   5. Passare a Progetto > Aggiungi nuovo elemento per creare nuovi file di codice oppure a Progetto > Aggiungi elemento esistente per aggiungere file di codice esistenti al progetto
//   6. Per aprire di nuovo questo progetto in futuro, passare a File > Apri > Progetto e selezionare il file con estensione sln
