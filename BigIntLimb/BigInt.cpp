#include "BigInt.h"
BigInt::BigInt() : sign(false), limbs({0u}){}

BigInt::BigInt(long long unsigned val, bool s) : sign(s)
{
	unsigned int base_mask = (1u << base) - 1u;
	//se ho una base 2^1 ogni limb sar� costituito dall'ultima cifra di val (maschera 000...001 = 1 = 2^1 - 1)
	//se ho una base 2^2 ogni limb sar� costituito dalle ultime 2 cifre di val (maschera 000...011 = 3 = 2^2 -1)
	while (val)
	{
		unsigned int limb = val & base_mask;
		limbs.push_back(limb);
		val >>= base;
	}
}

BigInt::BigInt(const std::vector<unsigned int> ls, bool s): limbs(ls), sign(s){}

//Per ora suppongo i numeri entrambi positivi
BigInt BigInt::operator+(const BigInt& other) const
{
	unsigned int carry = 0u;
	const BigInt* a = this;
	const BigInt* b = &other;
	size_t a_size = a->NumLimbs();
	size_t b_size = b->NumLimbs();
	
	std::vector<unsigned> res(std::max(a_size, b_size) + 1, 0);
	
	unsigned int base_mask = (1u << base) - 1u;
	int i = 0;
	
	for (; i < a_size && i < b_size; i++)
	{
		unsigned int sum = a->limbs[i] + b->limbs[i] + carry;
		res[i] = sum & base_mask;
		carry = sum >> base;		
	}

	for(; i < a_size; i++)
	{
		unsigned int sum = a->limbs[i] + 0u + carry;
		res[i] = sum & base_mask;
		carry = sum >> base;
	}

	for (;  i < b_size; i++)
	{
		unsigned int sum = b->limbs[i] + 0u + carry;
		res[i] = sum & base_mask;
		carry = sum >> base;
	}

	if (carry)
		res.push_back(carry);
	else
		res.pop_back();

	return BigInt(res);
	
}

//Per ora suppongo i numeri entrambi positivi
BigInt BigInt::operator-(const BigInt& other) const
{
	int carry = 0u;
	const BigInt* a = this;
	const BigInt* b = &other;
	size_t a_size = a->NumLimbs();
	size_t b_size = b->NumLimbs();

	std::vector<unsigned> res(std::max(a_size, b_size), 0);
	unsigned int base_mask = (1u << base) - 1u;
	int i = 0;

	for (; i < a_size && i < b_size; i++)
	{
		int diff = (int)a->limbs[i] - (int)b->limbs[i] - carry;
		res[i] = diff < 0 ? diff + (1 << base) : diff;
		carry = diff < 0;
	}

	for (; i < a_size; i++)
	{
		int diff = (int)a->limbs[i] - 0u - carry;
		res[i] = diff < 0 ? diff + (1 << base) : diff;
		carry = diff < 0;
	}

	for (; i < b_size; i++)
	{
		int diff = 0u - (int)b->limbs[i] - carry;
		res[i] = diff < 0 ? diff + (1 << base) : diff;
		carry = diff < 0;
	}
	while (res.size() > 1 && res.back() == 0)
		res.pop_back();
	if (carry)
		return ~(BigInt(res, carry) - 1);
	else
		return BigInt(res, carry);
}

bool BigInt::operator>(const BigInt& b) const
{
	return !(*this < b) && !(*this == b);
}
bool BigInt::operator>=(const BigInt& b) const
{
	return (*this == b) || (*this > b);
}
bool BigInt::operator<=(const BigInt& b) const
{
	return (*this == b) || (*this < b);
}

BigInt BigInt::operator~() const
{
	std::vector<unsigned int> resLimbs{ this->limbs.begin(), this->limbs.end() };
	size_t limbsSize = resLimbs.size();
	unsigned int base_mask = (1u << base) - 1u;
	for (int i = 0; i < limbsSize; i++)
	{
		(resLimbs[i] = ~this->limbs[i]) & base_mask;
	}
	return BigInt(resLimbs, sign);
}

BigInt BigInt::operator&(const BigInt& other) const
{
	const BigInt* a = this;
	const BigInt* b = &other;
	size_t a_size = a->NumLimbs();
	size_t b_size = b->NumLimbs();

	std::vector<unsigned> res(std::max(a_size, b_size), 0);
	unsigned int base_mask = (1u << base) - 1u;
	int i = 0;

	for (; i < a_size && i < b_size; i++)
	{
		res[i] = (a->limbs[i] & b->limbs[i]) & base_mask;
	}

	return BigInt(res, false);
}

BigInt BigInt::operator|(const BigInt& other) const
{
	const BigInt* a = this;
	const BigInt* b = &other;
	size_t a_size = a->NumLimbs();
	size_t b_size = b->NumLimbs();

	std::vector<unsigned> res(std::max(a_size, b_size), 0);
	unsigned int base_mask = (1u << base) - 1u;
	int i = 0;

	for (; i < a_size && i < b_size; i++)
	{
		res[i] = (a->limbs[i] | b->limbs[i]) & base_mask;
	}

	for (; i < a_size; i++)
	{
		res[i] = a->limbs[i];
	}

	for (; i < b_size; i++)
	{
		res[i] = b->limbs[i];
	}

	return BigInt(res, false);
}

bool BigInt::operator<(const BigInt& other) const
{
	if (this->sign != other.sign)
		return !sign;
	
	//stesso segno ma diverso numero di cifre
	size_t thisNumLimbs = this->limbs.size();
	size_t otherNumLimbs = other.limbs.size();

	if (thisNumLimbs != otherNumLimbs)
		return sign ? thisNumLimbs > otherNumLimbs : thisNumLimbs < otherNumLimbs;
	bool is_lower = false;
	for (int i = thisNumLimbs - 1; i >= 0 && !is_lower; i--)
		is_lower = sign ? limbs[i] > other.limbs[i] : limbs[i] < other.limbs[i];
	
	return is_lower;
}

bool BigInt::operator==(const BigInt& other) const
{
	if (this->sign != other.sign)
		return false;
	
	size_t thisNumLimbs = this->limbs.size();
	size_t otherNumLimbs = other.limbs.size();

	if (thisNumLimbs != otherNumLimbs)
		return false;

	bool is_equal = true;
	for (int i = thisNumLimbs - 1; i >= 0 && is_equal; i--)
		is_equal = limbs[i] == other.limbs[i];

	return is_equal;
}

BigInt BigInt::operator-() const
{
	return BigInt(this->limbs, !sign);
}


//devo prima implementare la divisione
std::ostream& operator<<(std::ostream& out, const BigInt& c)
{
	return out;
}


