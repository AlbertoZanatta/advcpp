#include "SmallObjAllocator.h"

SmallObjAllocator::SmallObjAllocator(size_t chunkSize, size_t maxObjectSize) : m_chunkSize(chunkSize), m_maxObjectSize(maxObjectSize), m_pLastAlloc(nullptr), m_pLastDealloc(nullptr), m_pool(0) {}

void* SmallObjAllocator::Allocate(size_t numBytes)
{
	if (m_pLastAlloc && m_pLastAlloc->BlockSize() == numBytes)
		return m_pLastAlloc->Allocate();

	FixedAllocator* allocator = FindFixedAllocator(numBytes);
	if (allocator == nullptr)
	{
		allocator = InsertFixedAllocator(numBytes);
	}
	m_pLastAlloc = allocator;
	return allocator->Allocate();
}

void SmallObjAllocator::Deallocate(void* p_block, size_t size)
{
	if (m_pLastDealloc && m_pLastDealloc->BlockSize() == size)
		return m_pLastDealloc->Deallocate(p_block);
	FixedAllocator* allocator = FindFixedAllocator(size);
	if (allocator == nullptr)
		return;
	m_pLastDealloc = allocator;
	allocator->Deallocate(p_block);
}

FixedAllocator* SmallObjAllocator::FindFixedAllocator(size_t size)
{
	FixedAllocsIter it = std::lower_bound(m_pool.begin(), m_pool.end(), size, Compare());
	if (it != m_pool.end() && it->BlockSize() == size)
		return &(*it);
	return nullptr;
}

FixedAllocator* SmallObjAllocator::InsertFixedAllocator(size_t size)
{
	FixedAllocator allocator;
	FixedAllocsIter it = std::lower_bound(m_pool.begin(), m_pool.end(), size, Compare());
	FixedAllocsIter pos = m_pool.insert(it, allocator);
	(*pos).Initialize(size, m_chunkSize);
	return &(*pos);
}