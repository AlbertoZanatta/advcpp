#pragma once

#include <vector>
#include <assert.h>
#include "Chunk.h"

class FixedAllocator
{
public:
	//TYPEDEFs
	//(i) Type of the container used to hold Chunks
	typedef std::vector<Chunk> Chunks;
	//(ii) Type of the iterator through the container of Chunks
	typedef Chunks::iterator ChunkIter;
	//(iii) Type of a const iterator through the container of Chunks
	typedef Chunks::const_iterator ChunkCIter;

public:
	//FixedAllocator constructor.
	FixedAllocator();
	//FixedAllocator destructor
	~FixedAllocator();
	//FixedAllocator initialization
	void Initialize(size_t blockSize, size_t chunkSize);
	//Returns pointer to the allocated BLOCK - or nullptr if it fails to allocate.
	void* Allocate();
	//Deallocate the BLOCK p is pointing to
	void Deallocate(void * p);
	
	//Returns a valid pointer if the block at address p is within a Chunk managed by the FixedAllocator
	Chunk * FindChunk(void * p) const;
	//const Chunk* HasBlock(void * p) const;
	
	//returns blocksize
	size_t BlockSize() const;
	//returns the number of blocks in each chunk
	unsigned char NumBlocks() const { return m_numBlocks; };
	//returns a const reference to the chunks array
	const Chunks& GetChunks() const { return m_chunks; }

public:
	//# of bytes in a single block managed by a Chunk
	size_t m_blockSize;
	//# of blocks managed by each Chunk
	unsigned char m_numBlocks;
	//vector of Chunks
	Chunks m_chunks;
	//Pointer to the last Chunk used for allocation
	Chunk* m_allocChunk;
	//Pointer to the last Chunk used for DEallocation
	Chunk* m_deallocChunk;
	//Pointer to an empty Chunk if there is one, nullptr otherwise
	Chunk* m_emptyChunk;
};
