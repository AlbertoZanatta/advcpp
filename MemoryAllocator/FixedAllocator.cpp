#include "FixedAllocator.h"

FixedAllocator::FixedAllocator() : m_blockSize(0), m_numBlocks(0), m_chunks(0), m_allocChunk(nullptr), m_deallocChunk(nullptr), m_emptyChunk(nullptr) {}

FixedAllocator::~FixedAllocator() 
{
	for (ChunkIter i(m_chunks.begin()); i != m_chunks.end(); ++i)
		i->Release();
}

void FixedAllocator::Initialize(size_t blockSize, size_t chunkSize)
{
	assert(blockSize > 0 && blockSize <= chunkSize);
	m_blockSize = blockSize;
	m_numBlocks = chunkSize / blockSize > UCHAR_MAX ? UCHAR_MAX : chunkSize / blockSize;
}

size_t FixedAllocator::BlockSize() const
{
	return m_blockSize;
}

void* FixedAllocator::Allocate()
{

	if (m_allocChunk == nullptr || m_allocChunk->m_blocksAvailable == 0)
	{
		Chunks::iterator it = m_chunks.begin();
		for (; it != m_chunks.end(); it++)
		{
			if (it->m_blocksAvailable > 0)
			{
				m_allocChunk = &*it;
				break;
			}
		}
		if (it == m_chunks.end())
		{
			m_chunks.reserve(m_chunks.size() + 1);
			Chunk newChunk;
			newChunk.Init(m_blockSize, m_numBlocks);
			m_chunks.push_back(newChunk);
			m_allocChunk = &m_chunks.back();
			m_deallocChunk = &m_chunks.back();
		}
	}

	assert(m_allocChunk != nullptr);
	assert(m_allocChunk->m_blocksAvailable > 0);
	if (m_allocChunk == m_emptyChunk)
		m_emptyChunk = nullptr;
	return m_allocChunk->Allocate(m_blockSize);
}

//const Chunk* FixedAllocator::HasBlock(void * p) const
//{
//	if (m_chunks.empty())
//		return nullptr;
//	if (m_deallocChunk != nullptr && m_deallocChunk->HasBlock(p, m_blockSize, m_numBlocks))
//		return m_deallocChunk;
//
//	for (ChunkCIter it = m_chunks.begin(); it != m_chunks.end(); it++)
//	{
//		if (it->HasBlock(p, m_blockSize, m_numBlocks))
//			return &*it;
//	}
//	return nullptr;
//
//}

Chunk* FixedAllocator::FindChunk(void* p_block) const
{
	if (m_chunks.empty())
		return nullptr;
	
	assert(m_deallocChunk);
	assert(m_allocChunk);

	if (m_deallocChunk->HasBlock(p_block, m_blockSize, m_numBlocks)) return m_deallocChunk;
	if (m_allocChunk->HasBlock(p_block, m_blockSize, m_numBlocks)) return m_allocChunk;

	//Creating 2 pointers: one corresponding to m_deallocChunk and the other pointing to the following Chunk;
	//The first will move backwards to the begin() iterator, the second forward to the end() iterator

	Chunk * lo = m_deallocChunk;
	Chunk * hi = m_deallocChunk + 1;
	const Chunk * loBound = &m_chunks.front();
	const Chunk * hiBound = &m_chunks.back();

	if (loBound == hiBound)
		return nullptr;
	for (; lo != nullptr && hi != nullptr;)
	{
		if (lo)
		{
			if (lo->HasBlock(p_block, m_blockSize, m_numBlocks))
				return lo;
			if (lo == loBound)
				lo = nullptr;
			else
				lo--;
		}
		if (hi)
		{
			if (hi->HasBlock(p_block, m_blockSize, m_numBlocks))
				return hi;
			if (hi == hiBound)
				hi = nullptr;
			else
				hi++;
		}
	}

	return nullptr;
}

void FixedAllocator::Deallocate(void * p_block)
{
	//Devo capire a che Chunk appartiene il puntatore
	Chunk* holder = FindChunk(p_block);
	//Chiamo Deallocate su quel Chunk
	if (holder == nullptr)
		return;

	holder->Deallocate(p_block, m_blockSize);
	m_deallocChunk = holder;
	if (holder->IsEmpty(m_numBlocks))
	{
		if (m_emptyChunk)
		{
			m_emptyChunk->Release();
			m_chunks.pop_back();
		}
		
		std::iter_swap(holder, &m_chunks.back());
		m_deallocChunk = &m_chunks.back();
		m_emptyChunk = &m_chunks.back();
	}
}
