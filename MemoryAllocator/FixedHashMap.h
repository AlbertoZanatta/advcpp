#pragma once
template<class T, size_t SIZE = 1u << 16u>
class FixedHashMap
{
public:
	T* map = new T[SIZE]{};
	T& operator[](size_t key)
	{
		return map[key];
	}

	size_t Size()
	{
		return SIZE;
	}

	size_t Hash(size_t val)
	{
		return val & (SIZE - 1);
	}
};