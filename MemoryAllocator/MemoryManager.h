#pragma once

#include "SmallObjAllocator.h"
#include "GeneralObjectAllocator.h"
#include "FixedHashMap.h"
#include "FreeList.h"
#include <iostream>
struct AllocDesc
{
	void* Ptr; //Allocation address
	unsigned long Size; //Allocation size
	AllocDesc* Next; //Next allocation in the HashTable
};

class MemoryManager
{
public:
	void* Allocate(size_t);
	void Free(void* p, size_t size);
	MemoryManager();
	~MemoryManager() = default;
	MemoryManager(const MemoryManager&) = default;
	MemoryManager& operator=(const MemoryManager&) = default;

	void RegisterAlloc(void* Ptr, size_t Size);

	void UnregisterAlloc(void* Ptr, size_t Size);

	void DumpAllocs();
	

private:	
	SmallObjAllocator soa;
	GeneralObjectAllocator goa;
	FixedHashMap<AllocDesc*> Hash;
	FreeList<AllocDesc> FreeAllocs;
};

//#pragma once

