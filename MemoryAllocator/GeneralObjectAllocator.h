#pragma once
#include <cstdint>
#include <utility>
#include <errno.h>
#include <stdio.h>
class GeneralObjectAllocator
{
public:
	static const size_t MAX_HEAP = 1u << 16u; // 2 ^ 16
	//Word and header/footer size (bytes)
	static const size_t WORD_SIZE = sizeof(size_t);
	//Double word size (bytes)
	static const size_t DWORD_SIZE = 2u * sizeof(size_t);
	//Extend heap by this amount
	static const size_t CHUNK_SIZE = 1u << 7u;

	char* startHeap;
	char* endHeap;
	char* maxHeap;

	char* heapList;
	
	//Combines a size and an allocate bit and returns a value that can be stored in a header/footer
	inline size_t Pack(size_t size, size_t allocation){ return (size | allocation);}

	//Reads and returns the word referenced by argument p
	inline size_t GetWord(void* p) { size_t* p_u = static_cast<size_t*>(p); return *p_u; }

	//Stores val in the word pointed at by argument p
	inline void PutWord(void* p, size_t val) { size_t* p_u = static_cast<size_t*>(p);  *p_u = val; }

	//Returns the size info of an header/footer at address p
	inline size_t GetSize(void* p) { return GetWord(p) & ~(DWORD_SIZE - 1); }

	//Returns the size info of an header/footer at address p
	inline size_t GetAlloc(void* p) { return GetWord(p) & 1; }

	//Given a blockpointer bp - which points to the first payload byte of a block - returns the pointer to the block header
	inline char* HeaderPointer(void* bp) { return static_cast<char*>(bp) - WORD_SIZE; }

	//Given a blockpointer bp - which points to the first payload byte of a block - returns the pointer to the block footer
	inline char* FooterPointer(void* bp) { return static_cast<char*>(bp) + GetSize(HeaderPointer(bp)) - DWORD_SIZE; }

	//Given a blockpointer bp - which points to the first payload byte of a block - returns the block pointer of the next block
	inline char* NextBlockPointer(void* bp) { return static_cast<char*>(bp) + GetSize(static_cast<char*>(bp) - WORD_SIZE) ; }

	//Given a blockpointer bp - which points to the first payload byte of a block - returns the block pointer of the previous block
	inline char* PrevBlockPointer(void* bp) { return static_cast<char*>(bp) - GetSize(static_cast<char*>(bp) - DWORD_SIZE); }

	//Max utility function
	inline size_t Max(size_t a, size_t b) { return a > b ? a : b; }
	

	//Initialize the memory system model
	void InitHeap();

	//Initialize the free list
	int Init();

	//Extend the heap by incr bytes and returns the start address of the new area.
	void* ExtendHeap(unsigned int incr);

	//Extend the free list
	void* Extend(size_t words);

	//Coalesces *bp* with either the previous or next blocks (if not used)
	void* Coalesce(void* bp);

	void Free(void* bp);	//Frees a memory block


	//Allocates a new block with a payload of at least *size*
	void* Allocate(size_t size);

	//Performs a first-fit search of the implicit free list and returns a suitable block if available.
	void* FirstFit(size_t asize);

	//Places the requested block at the beginning of the free block, splitting only if the size 
	//of the remainder would equal or exceed the minimum block size;
	void Place(void* bp, size_t asize);


};