#pragma once
#include "MemoryManager.h"

#ifdef USE_MM
MemoryManager mm;
void* operator new(size_t size){ return mm.Allocate(size);}
void operator delete(void* p, size_t size) { return mm.Free(p, size); }
#endif // USE_MM

