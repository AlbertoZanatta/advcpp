#include "Chunk.h"
#include <assert.h>
#include <cstdlib>
//Chunk is a POD structure manipulated ONLY by the FixedAllocator
//Chunk contains and manages a chunk of memory consisting of an integral number of fixed-size BLOCKS
void Chunk::Init(size_t blockSize, unsigned char numBlocks)
{
	void* chunkMemory = malloc(blockSize * numBlocks);
	m_pData = static_cast<unsigned char*>(chunkMemory);
	m_firstAvailableBlock = 0;
	m_blocksAvailable = numBlocks;
	unsigned char i = 0;
	unsigned char* p = m_pData;
	//The FIRST BYTE of each block is used to store the index of the NEXT FREE BLOCK
	for (; i != m_blocksAvailable; p += blockSize)
	{
		*p = ++i;
	}
}

void* Chunk::Allocate(size_t blockSize)
{
	if (IsFilled()) return nullptr;
	unsigned char* p_FirstAvailBlock = m_pData + (blockSize * m_firstAvailableBlock);
	m_firstAvailableBlock = *p_FirstAvailBlock;
	m_blocksAvailable--;
	return p_FirstAvailBlock;
}

void Chunk::Deallocate(void* p, size_t blockSize)
{
	assert(p >= m_pData);
	unsigned char* p_BlockToDealloc = static_cast<unsigned char*>(p);
	assert((p_BlockToDealloc - m_pData) % blockSize == 0);
	*p_BlockToDealloc = m_firstAvailableBlock;
	unsigned char* p_FirstAvailBlock = m_pData + (blockSize * m_firstAvailableBlock);
	unsigned char blockToDeallocIndex = (p_BlockToDealloc - m_pData) / blockSize;
	m_firstAvailableBlock = blockToDeallocIndex;
	assert(m_firstAvailableBlock == (p_BlockToDealloc - m_pData) / blockSize);	
	m_blocksAvailable++;
}

void Chunk::Release()
{
	assert(m_pData != nullptr);
	free(m_pData);
}

bool Chunk::HasBlock(void * p, size_t blockSize, unsigned char numBlocks) const
{
	size_t chunkLength = blockSize * numBlocks;
	unsigned char * pc = static_cast<unsigned char *>(p);
	return (m_pData <= pc) && (pc < m_pData + chunkLength);
}

bool Chunk::IsBlockAvailable(void * p, unsigned char numBlocks, size_t blockSize) const
{
	if (IsFilled())
		return false;
	
	unsigned char * p_Block = static_cast<unsigned char *>(p);
	assert((p_Block - m_pData) % blockSize == 0);
	unsigned char blockIndex = (p_Block - m_pData) / blockSize;

	unsigned char index = m_firstAvailableBlock;
	unsigned char* indexPointer = m_pData + index * blockSize;
	for (unsigned char blockCounter = 0; blockCounter < m_blocksAvailable; blockCounter++)
	{
		if (blockIndex == index)
			return true;
		index = *indexPointer;
		indexPointer = m_pData + index * blockSize;
	}

	return false;


}

bool Chunk::IsFilled() const
{
	return m_blocksAvailable == 0;
}

bool Chunk::IsEmpty(unsigned char numBlocks) const
{
	return m_blocksAvailable == numBlocks;
}
