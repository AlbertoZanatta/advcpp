#include "MemoryManager.h"
#include <cstdlib>
#include <iostream>

MemoryManager::MemoryManager() 
{ 
	goa.Init();  
	std::cout << "Memory Manager constr" << std::endl; 
}
void MemoryManager::RegisterAlloc(void* Ptr, size_t Size)
{
	//Allocate a new AllocDesc from the freelist
	AllocDesc* newAllocDesc = FreeAllocs.New();

	//Set the allocation info
	newAllocDesc->Ptr = Ptr;
	newAllocDesc->Size = Size;

	size_t hashedPtr = Hash.Hash(reinterpret_cast<size_t>(Ptr));
	newAllocDesc->Next = Hash[hashedPtr];
	Hash[hashedPtr] = newAllocDesc;
}

void MemoryManager::UnregisterAlloc(void* Ptr, size_t Size)
{
	size_t hashedPtr = Hash.Hash(reinterpret_cast<size_t>(Ptr));

	AllocDesc* allocDesc = Hash[hashedPtr];
	AllocDesc dummy; dummy.Next = allocDesc;
	AllocDesc* prevAllocDesc = &dummy;

	while (allocDesc != nullptr)
	{
		if (allocDesc->Ptr == Ptr)
		{
			prevAllocDesc->Next = allocDesc->Next;
			break;
		}
		allocDesc = allocDesc->Next;
		prevAllocDesc = prevAllocDesc->Next;
	}

	if (allocDesc != nullptr)
	{
		if (allocDesc == Hash[hashedPtr])
		{
			Hash[hashedPtr] = allocDesc->Next;
		}
		FreeAllocs.Delete(allocDesc);
	}
}

void MemoryManager::DumpAllocs()
{
	printf("Address, size\n");
	for (size_t i = 0; i < Hash.Size(); i++)
	{
		AllocDesc* alloc = Hash[i];
		while (alloc)
		{
			printf("0x%08x,%d\n", alloc->Ptr, alloc->Size);
			alloc = alloc->Next;
		}
	}
}

void* MemoryManager::Allocate(size_t size)
{
	void* p = nullptr;
	//std::cout << std::string(file) << ":" << line << std::endl;
	if (size <= 64)
	{
		std::cout << "Small Object Allocator" << std::endl;
		p = soa.Allocate(size);
	}		
	else
	{
		std::cout << "General Object allocation" << std::endl;
		p = goa.Allocate(size);
	}
		
	if (p)
	{
		RegisterAlloc(p, size);
		return p;
	}
	else
		throw std::bad_alloc();
}

void MemoryManager::Free(void* p, size_t size)
{
	UnregisterAlloc(p, size);
	if (size <= 64)
	{
		std::cout << "Small Object Deallocation" << std::endl;
		soa.Deallocate(p, size);
	}
	else
	{
		std::cout << "General Object Deallocation" << std::endl;
		goa.Free(p);
	}
	
}

//MemoryManager& MemoryManager::Instance()
//{
//	std::cout << "GetInstance" << std::endl;
//	static MemoryManager instance{};
//	return instance;
//}