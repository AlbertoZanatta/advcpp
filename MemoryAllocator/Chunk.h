#pragma once

struct Chunk
{
	//Initializes a Chunk object.
	void Init(size_t blockSize, unsigned char blocks);

	//Allocates a BLOCK. The 'blockSize' is passed explicitly: the block size is known at a superior level and keeping
	//a member is redundant and wastes space.
	void* Allocate(size_t blockSize);

	//Deallocates a BLOCK
	void Deallocate(void* p, size_t blockSize);

	// Releases the allocated block of memory.
	void Release();

	//Returns true if block at address p is inside this Chunk.
	bool HasBlock(void* p, size_t blockSize, unsigned char numBlocks) const;

	//Returns true if the BLOCK at address p managed by the Chunk is available for Allocation
	bool IsBlockAvailable(void * p, unsigned char numBlocks,size_t blockSize) const;

	//Returns true if NO BLOCK is available for Allocation 
	bool IsFilled() const;

	//Returns true if ALL BLOCKS are available for Allocation
	bool IsEmpty(unsigned char numBlocks) const;

	//For efficiency reasons Chunk does NOT define constr/destr/assignment opr

	//Pointer to the MANAGED MEMORY
	unsigned char* m_pData;

	//Index of the first block available in the chunk
	unsigned char m_firstAvailableBlock;

	//The # of BLOCKS available in the Chunk. [0, UCHARMAX (usually 255)]
	unsigned char m_blocksAvailable;
};
