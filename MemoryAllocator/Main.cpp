// MemoryAllocator.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <iostream>
#include <vector>
#include <algorithm>
#include "MemoryManager.h"
#include "Chunk.h"
#include "FixedAllocator.h"
#include "SmallObjAllocator.h"
#include "GeneralObjectAllocator.h"
#include "FixedHashMap.h"
#include "STLAllocator.h"
#include <assert.h>

void _f(const char* File, unsigned long Line)
{
	std::cout << std::string(File) << ":" << Line << std::endl;
}

#define f() _f(__FILE__, __LINE__);
#define USE_MM


//#include "use_memory_manager.h"
//void* operator new(size_t size) { return mm.Allocate(size); }
//void operator delete(void* p, size_t size) { return mm.Free(p, size); }

class C
{
public:
	static C& GetInstance() { static C c; return c; }
private:
	std::string s;
	C() { std::cout << "str constr" << std::endl; };
	~C() = default;
	C(const C&) = default;
	C& operator=(const C&) = default;
};



void TestChunk()
{
	Chunk chunk;
	size_t blockSize = 2;
	unsigned char numBlocks = 5;
	chunk.Init(blockSize, numBlocks);
	assert(chunk.IsEmpty(numBlocks) && !chunk.IsFilled() && chunk.m_blocksAvailable == numBlocks);
	void* ptr_0 = chunk.Allocate(blockSize); //First block
	new(ptr_0) char('a');
	void* ptr_1 = chunk.Allocate(blockSize); //Second block
	new(ptr_1) char('b');
	void* ptr_2 = chunk.Allocate(blockSize); //Third block
	new(ptr_2) char('c');
	void* ptr_3 = chunk.Allocate(blockSize); //Fourth block
	new(ptr_3) char('d');
	void* ptr_4 = chunk.Allocate(blockSize); //Fifth block
	new(ptr_4) char('e');
	assert(!chunk.IsEmpty(numBlocks) && chunk.IsFilled() && chunk.m_blocksAvailable == 0);
	void* ptr_5 = chunk.Allocate(blockSize); //This should be null (6th block doeasn't exist)
	assert(!chunk.IsEmpty(numBlocks) && chunk.IsFilled() && chunk.m_blocksAvailable == 0 && ptr_5 == nullptr);
	assert(!chunk.IsBlockAvailable(ptr_2, numBlocks, blockSize));
	chunk.Deallocate(ptr_2, blockSize); //Now first available bloack is index 2
	assert(chunk.IsBlockAvailable(ptr_2, numBlocks, blockSize));
	assert(!chunk.IsEmpty(numBlocks) && !chunk.IsFilled() && chunk.m_blocksAvailable == 1 && chunk.m_firstAvailableBlock == 2);
	chunk.Deallocate(ptr_0, blockSize); //Now first available bloack is index 0
	assert(!chunk.IsEmpty(numBlocks) && !chunk.IsFilled() && chunk.m_blocksAvailable == 2 && chunk.m_firstAvailableBlock == 0 && *static_cast<unsigned char*>(ptr_0) == 2);
	assert(!chunk.IsBlockAvailable(ptr_4, numBlocks, blockSize));
	chunk.Deallocate(ptr_4, blockSize);
	assert(chunk.IsBlockAvailable(ptr_4, numBlocks, blockSize));
	assert(!chunk.IsEmpty(numBlocks) && !chunk.IsFilled() && chunk.m_blocksAvailable == 3 && chunk.m_firstAvailableBlock == 4 && *static_cast<unsigned char*>(ptr_4) == 0);
	assert(chunk.HasBlock(ptr_0, blockSize, numBlocks) 
		&& chunk.HasBlock(ptr_1, blockSize, numBlocks) 
		&& chunk.HasBlock(ptr_2, blockSize, numBlocks) 
		&& chunk.HasBlock(ptr_3, blockSize, numBlocks) 
		&& chunk.HasBlock(ptr_0, blockSize, numBlocks) 
		&& !chunk.HasBlock(ptr_5, blockSize, numBlocks));
}

void TestFixedAllocator()
{
	FixedAllocator fa;
	size_t blockSize = 2;
	unsigned char numBlocks = 3;
	fa.Initialize(blockSize, 3 * 2); //Chunk con 2 byte di spazio e organizzati in 3 blocchi
	assert(fa.BlockSize() == blockSize && fa.NumBlocks() == numBlocks && fa.GetChunks().size() == 0);
	void* ptr_0 = fa.Allocate();
	new(ptr_0) char('a');
	void* ptr_1 = fa.Allocate();
	new(ptr_1) char('b');
	void* ptr_2 = fa.Allocate();
	new(ptr_2) char('c');
	assert(fa.m_chunks.size() == 1 && fa.m_deallocChunk == fa.m_allocChunk && fa.m_deallocChunk == &fa.m_chunks[0]);
	assert(fa.FindChunk(ptr_0) == &fa.m_chunks[0]);
	assert(fa.FindChunk(ptr_1) == &fa.m_chunks[0]);
	assert(fa.FindChunk(ptr_2) == &fa.m_chunks[0]);
	void* ptr_3 = fa.Allocate(); //Fourth block
	new(ptr_3) char('d');
	assert(fa.m_chunks.size() == 2 && fa.m_deallocChunk == &fa.m_chunks[1] && fa.m_allocChunk == &fa.m_chunks[1]);
	assert(fa.FindChunk(ptr_0) == &fa.m_chunks[0]);
	assert(fa.FindChunk(ptr_1) == &fa.m_chunks[0]);
	assert(fa.FindChunk(ptr_2) == &fa.m_chunks[0]);
	assert(fa.FindChunk(ptr_3) == &fa.m_chunks[1]);
	fa.Deallocate(ptr_3);
	assert(fa.m_chunks.size() == 2);
	assert(fa.m_chunks[0].IsFilled());
	assert(fa.m_chunks[1].IsEmpty(numBlocks));
	assert(fa.m_emptyChunk != nullptr);
	assert(fa.m_emptyChunk == &fa.m_chunks[1]);
	fa.Deallocate(ptr_1);
	assert(fa.m_chunks.size() == 2 && !fa.m_chunks[0].IsFilled());
	void* ptr_0_2 = fa.Allocate();
	new(ptr_0_2) char('A');
	assert(fa.FindChunk(ptr_0_2) == &fa.m_chunks[1]);
	void* ptr_1_2 = fa.Allocate();
	new(ptr_1_2) char('B');
	assert(fa.FindChunk(ptr_1_2) == &fa.m_chunks[1]);
	void* ptr_2_2 = fa.Allocate();
	new(ptr_2_2) char('C');
	assert(fa.FindChunk(ptr_2_2) == &fa.m_chunks[1]);
	void* ptr_3_2 = fa.Allocate();
	new(ptr_3_2) char('D');
	assert(fa.FindChunk(ptr_3_2) == &fa.m_chunks[0]);
	assert(fa.m_allocChunk == &fa.m_chunks[0]);
	fa.Deallocate(ptr_0);
	fa.Deallocate(ptr_2);
	fa.Deallocate(ptr_3_2);
	//Adesso il primo chunk � vuoto ed � spostato in fondo alla lista
	assert(fa.m_emptyChunk == &fa.m_chunks[1]);
	//I puntatori con 'A', 'B' e 'C' sono ora nel primo chunk (chunk di indice zero)
	assert(fa.FindChunk(ptr_0_2) == &fa.m_chunks[0]);
	assert(fa.FindChunk(ptr_1_2) == &fa.m_chunks[0]);
	assert(fa.FindChunk(ptr_2_2) == &fa.m_chunks[0]);
}

void TestSmallObjAllocator()
{
	SmallObjAllocator soalloc(6,3);
	soalloc.InsertFixedAllocator(3);
	soalloc.InsertFixedAllocator(2);
	soalloc.InsertFixedAllocator(1);

	FixedAllocator* res3 = soalloc.FindFixedAllocator(3);
	FixedAllocator* res2 = soalloc.FindFixedAllocator(2);
	FixedAllocator* res1 = soalloc.FindFixedAllocator(1);
	assert(res3->BlockSize() == 3);
	assert(res2->BlockSize() == 2);
	assert(res1->BlockSize() == 1);
}

void TestGeneralAllocator()
{
	//0. Elementi base:
	GeneralObjectAllocator goa;
	assert(GeneralObjectAllocator::DWORD_SIZE == 2 * sizeof(size_t));
	assert(GeneralObjectAllocator::WORD_SIZE == sizeof(size_t));
	goa.Init();
	void* p = goa.Allocate(32);
	int* i = new(p) int(13);
	goa.Free(p);

}

int main()
{
	std::cout << "testing Custom Allocator class" << std::endl;
	
	FixedHashMap<int> Hash;
	Hash[4] = 2;

	//TestGeneralAllocator();
	MemoryManager mm;
	void* p = mm.Allocate(sizeof(int));
	int* pi = new(p) int(4);
	mm.DumpAllocs();
	mm.Free(p, sizeof(int));
	mm.DumpAllocs();
	//f();
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
