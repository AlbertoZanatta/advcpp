#include "GeneralObjectAllocator.h"

void GeneralObjectAllocator::InitHeap()
{
	startHeap = static_cast<char*>(malloc(MAX_HEAP));
	endHeap = startHeap;
	maxHeap = startHeap + MAX_HEAP;
}

int GeneralObjectAllocator::Init()
{
	InitHeap();
	void* beginHeap = ExtendHeap(4u * WORD_SIZE);
	if (beginHeap == (void*)-1)
		return -1;
	heapList = static_cast<char*>(beginHeap);
	PutWord(heapList, 0); //Alignment padding
	PutWord(heapList + (1u * WORD_SIZE), Pack(DWORD_SIZE, 1)); //Prologue header
	PutWord(heapList + (2u * WORD_SIZE), Pack(DWORD_SIZE, 1)); //Prologue header
	PutWord(heapList + (3u * WORD_SIZE), Pack(0, 1)); //EpilogueHeader
	heapList += 2u * WORD_SIZE;
	//Extend the freeList with a free block of CHUNK_SIZE bytes
	if (Extend(CHUNK_SIZE / WORD_SIZE) == nullptr)
		return -1;
	return 0;
}

void* GeneralObjectAllocator::ExtendHeap(unsigned int incr)
{
	char* oldEndHeap = endHeap;
	if (endHeap + incr > maxHeap)
	{
		errno = ENOMEM;
		fprintf(stderr, "Error: ExtendHeap failed. Ran out of memory \n");
		return reinterpret_cast<void*>(-1);
	}
	endHeap += incr;
	return oldEndHeap;
}

void* GeneralObjectAllocator::Extend(size_t words)
{
	char* bp;
	size_t size;

	//Allocate an even number of words to mantain alignment
	size = words % 2 ? (words + 1) * WORD_SIZE : words * WORD_SIZE;
	void* oldEndHeap = ExtendHeap(size);
	if (oldEndHeap == (void*)-1) return nullptr;
	bp = static_cast<char*>(oldEndHeap);

	//Initialize free block header/footer and the epilogue header
	PutWord(HeaderPointer(bp), Pack(size, 0)); //Free block header
	PutWord(FooterPointer(bp), Pack(size, 0)); //Free block footer
	PutWord(HeaderPointer(NextBlockPointer(bp)), Pack(0, 1));
	/* TO DO */
	//Coalesce if previous block was free
	return Coalesce(bp);
}

void* GeneralObjectAllocator::Coalesce(void* bp)
{
	size_t prev_alloc = GetAlloc(FooterPointer(PrevBlockPointer(bp)));
	size_t next_alloc = GetAlloc(HeaderPointer(NextBlockPointer(bp)));
	size_t size = GetSize(HeaderPointer(bp));

	//Case 1: both PREV and NEXT block are being USED
	if (prev_alloc && next_alloc)
		return bp;

	//Case 2: PREV block is USED, but NEXT block is FREE
	else if (prev_alloc && !next_alloc)
	{
		size += GetSize(HeaderPointer(NextBlockPointer(bp)));
		PutWord(HeaderPointer(bp), Pack(size, 0));
		PutWord(FooterPointer(bp), Pack(size, 0));
	}

	//Case 3: PREV block is FREE, but NEXT block is USED
	else if (!prev_alloc && next_alloc)
	{
		size += GetSize(HeaderPointer(PrevBlockPointer(bp)));
		PutWord(FooterPointer(bp), Pack(size, 0));
		PutWord(HeaderPointer(PrevBlockPointer(bp)), Pack(size, 0));
		bp = PrevBlockPointer(bp);

	}

	//Case 4: both PREV and NEXT block are FREE
	else if (!prev_alloc && !next_alloc)
	{
		size += GetSize(HeaderPointer(PrevBlockPointer(bp))) + GetSize(FooterPointer(NextBlockPointer(bp)));
		PutWord(HeaderPointer(PrevBlockPointer(bp)), Pack(size, 0));
		PutWord(FooterPointer(NextBlockPointer(bp)), Pack(size, 0));
		bp = PrevBlockPointer(bp);
	}
	return bp;
}

void GeneralObjectAllocator::Free(void* bp)
{
	size_t size = GetSize(HeaderPointer(bp));
	PutWord(HeaderPointer(bp), Pack(size, 0));
	PutWord(FooterPointer(bp), Pack(size, 0));
	Coalesce(bp);
}

void* GeneralObjectAllocator::Allocate(size_t size)
{
	size_t asize; //Adjusted block size
	size_t extendedSize; //Amount to extend heap if no fit
	char* bp;

	//Ignore invalid requests
	if (size == 0) return nullptr;

	//Adjust block size to include overhead and alignment requirements
	if (size <= DWORD_SIZE)
		asize = 2 * DWORD_SIZE;
	else
		asize = DWORD_SIZE * ((size + (DWORD_SIZE - 1) + DWORD_SIZE) / DWORD_SIZE);

	/*TO DO - findfit and place*/
	void* firstFitBlock = FirstFit(asize);
	if (firstFitBlock != nullptr)
	{
		bp = static_cast<char*>(firstFitBlock);
		Place(bp, asize);
		return bp;
	}

	extendedSize = Max(asize, CHUNK_SIZE);
	void* extendedBlock = Extend(extendedSize / WORD_SIZE);
	if (extendedBlock == nullptr)
		return nullptr;
	bp = static_cast<char*>(extendedBlock);
	Place(bp, asize);
	return bp;
}

void* GeneralObjectAllocator::FirstFit(size_t asize)
{
	char* pb;
	for (pb = heapList; GetSize(HeaderPointer(pb)) > 0; pb = NextBlockPointer(pb))
	{
		if (!GetAlloc(HeaderPointer(pb)) && GetSize(HeaderPointer(pb)) >= asize)
			return pb;
	}
	return nullptr;
}

void GeneralObjectAllocator::Place(void* bp, size_t asize)
{
	size_t csize = GetSize(HeaderPointer(bp));
	if ((csize - asize) >= 2 * DWORD_SIZE)
	{
		PutWord(HeaderPointer(bp), Pack(asize, 1));
		PutWord(FooterPointer(bp), Pack(asize, 1));
		bp = NextBlockPointer(bp);
		PutWord(HeaderPointer(bp), Pack(csize - asize, 0));
		PutWord(FooterPointer(bp), Pack(csize - asize, 0));
	}
	else
	{
		PutWord(HeaderPointer(bp), Pack(csize, 1));
		PutWord(FooterPointer(bp), Pack(csize, 1));
	}
}
