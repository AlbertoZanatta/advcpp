#pragma once
//https://jfdube.wordpress.com/2011/10/06/memory-management-part-2-allocations-tracking/
template<class T> class FreeList
{
	T* Free;
public:
	FreeList() : Free(nullptr) {}

	inline T* New()
	{
		if (!Free)
		{
			const unsigned long AllocSize = 65536;
			const unsigned long NumAllocPerBatch = AllocSize / sizeof(T);
			T* AllocBatch = static_cast<T*>(malloc(AllocSize));
			for (unsigned long i = 0; i < NumAllocPerBatch; i++)
			{
				Delete(AllocBatch++);
			}
		}
		T* Result = Free;
		Free = *(T**)Result;
		return Result;
	}

	inline void Delete(T* Ptr)
	{
		*(T**)Ptr = Free;
		Free = Ptr;
	}


};
