#pragma once
#include "FixedAllocator.h"
#include <vector>
class SmallObjAllocator
{
public:
	SmallObjAllocator(size_t chunkSize = 4096, size_t maxObjectSize = 64);
	void* Allocate(size_t numBytes);
	void Deallocate(void* p_block, size_t size);
	size_t MaxObjSize() const { return m_maxObjectSize; }

	FixedAllocator* FindFixedAllocator(size_t size);
	FixedAllocator* InsertFixedAllocator(size_t size);
	std::vector<FixedAllocator> m_pool;
	FixedAllocator* m_pLastAlloc;
	FixedAllocator* m_pLastDealloc;

	size_t m_chunkSize;
	size_t m_maxObjectSize;

	typedef struct Compare
	{
		bool operator()(const FixedAllocator& allocator, const size_t& blockSize) const{	return allocator.BlockSize() < blockSize;	}
		bool operator()(const size_t& blockSize, const FixedAllocator& allocator) const{	return blockSize < allocator.BlockSize();	}
	} Compare;

	//TYPEDEFs
	//(i) Type of the container used to hold Chunks
	typedef std::vector<FixedAllocator> FixedAllocs;
	//(ii) Type of the iterator through the container of Chunks
	typedef FixedAllocs::iterator FixedAllocsIter;
	//(iii) Type of a const iterator through the container of Chunks
	typedef FixedAllocs::const_iterator FixedAllocsCIter;

};
