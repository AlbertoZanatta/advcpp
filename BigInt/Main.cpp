// BigInt.cpp : Questo file contiene la funzione 'main', in cui inizia e termina l'esecuzione del programma.
//

#include <iostream>
#include "BigInt.h"
#include <limits.h>
#include <chrono>

long long int exp_by_square_util(int mul, int x, int n)
{
    if (n == 0)
        return 1;
    if (n == 1)
        return x * mul;
    int pow2x = x * x;
    int newMul = n % 2 ? mul * x : mul;
    int newExp = n % 2 ? (n - 1) / 2 : n / 2;
    return exp_by_square_util(newMul, pow2x, newExp);
}

long long int exp_by_square_iter(long long x, long long n)
{
    if (n == 0)
        return 1;
    long long y = 1;
    while (n > 1)
    {
        x = x * x;
        y = n % 2 ? x * y : y;
        n = n % 2 ? (n - 1) / 2 : n / 2;
    }
    return x * y;
    /*
    if n = 0 then return 1
    y := 1;
    while n > 1 do
      if n is even then
        x := x * x;
        n := n / 2;
      else
        y := x * y;
        x := x * x;
        n := (n � 1) / 2;
    return x * y
    */
}

int exp_by_square(int x, int n)
{
    return exp_by_square_util(1, x, n);
    //if (n == 0)
    //    return 1;
    //if (n == 1)
    //    return x;
    //int pow2x = x * x;
    //int newMul = n % 2 ?  x : 1;
    //int newExp = n % 2 ? (n - 1) / 2 : n / 2;
    //return newMul * exp_by_square(pow2x, newExp);
}

long long int stupid_exp(int x, int n)
{
    long long int res = 1;
    while (n--)
    {
        res *= x;
    }

    return res;

}

//int main()
//{
//    auto t1 = std::chrono::high_resolution_clock::now();
//    std::cout << exp_by_square(1, 10000000) << std::endl;
//    auto t2 = std::chrono::high_resolution_clock::now();
//    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
//    std::cout << duration << std::endl;
//
//    t1 = std::chrono::high_resolution_clock::now();
//    std::cout << exp_by_square_iter(1, 10000000) << std::endl;
//    t2 = std::chrono::high_resolution_clock::now();
//    duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
//    std::cout << duration << std::endl;
//
//    t1 = std::chrono::high_resolution_clock::now();
//    std::cout << stupid_exp(1, 10000000) << std::endl;
//    t2 = std::chrono::high_resolution_clock::now();
//    duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
//    std::cout << duration << std::endl;
//    return 0;
//}
void f(int v[])
{
    std::cout << sizeof(v) << std::endl;
}
int main()
{
    
    BigInt u = "943253454637657547";
    BigInt v = "958764532435465867";
    BigInt c = 92228191918;
    std::cout << u << std::endl;
    std::cout << v << std::endl;
    std::cout << c << std::endl;
    //std::cout << (u == v) << std::endl;
    //std::cout << (u == u) << std::endl;
    auto t1 = std::chrono::high_resolution_clock::now();
    //std::cout << u.fast_fast_mul(v) << std::endl;
    //u += v;
    std::cout << (u += v) << std::endl;
    auto t2 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
    std::cout << duration << std::endl;

    t1 = std::chrono::high_resolution_clock::now();
    //std::cout << (BigInt::slow_power(u, 25600)) << std::endl;
    BigInt result = BigInt::Power(u, 10000);
    //std::cout << result << std::endl;
    //u * v;
    t2 = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
    std::cout << duration << std::endl;

   /* BigInt v = "3";
    std::vector<int> vec(2, 10);

    std::cout <<  (u%v)  << std::endl;
    std::cout << std::numeric_limits<char>::digits10 << std::endl;*/
}

// Per eseguire il programma: CTRL+F5 oppure Debug > Avvia senza eseguire debug
// Per eseguire il debug del programma: F5 oppure Debug > Avvia debug

// Suggerimenti per iniziare: 
//   1. Usare la finestra Esplora soluzioni per aggiungere/gestire i file
//   2. Usare la finestra Team Explorer per connettersi al controllo del codice sorgente
//   3. Usare la finestra di output per visualizzare l'output di compilazione e altri messaggi
//   4. Usare la finestra Elenco errori per visualizzare gli errori
//   5. Passare a Progetto > Aggiungi nuovo elemento per creare nuovi file di codice oppure a Progetto > Aggiungi elemento esistente per aggiungere file di codice esistenti al progetto
//   6. Per aprire di nuovo questo progetto in futuro, passare a File > Apri > Progetto e selezionare il file con estensione sln
