#include "BigInt.h"

#pragma region constructors
BigInt::BigInt() : m_number({ 0 }), m_isnegative(false) {}

BigInt::BigInt(std::vector<int> number, bool sign) : m_number(number), m_isnegative(sign) {}

BigInt::BigInt(std::vector<long long> number, bool sign) : m_number(number.size(), 0), m_isnegative(sign)
{
	for (size_t i = 0; i < number.size(); i++)
		m_number[i] = number[i];
}

BigInt::BigInt(long long int num) : m_number(0), m_isnegative(num < 0)
{
	if (num < 0)
	{
		num = -num;
	}
	int base = std::pow(10, m_base_size);
	while (num)
	{
		m_number.push_back(num % base);
		num /= base;
	}
}

BigInt::BigInt(const char* num) : BigInt(std::string(num)) {}

BigInt::BigInt(std::string s) : m_number(0), m_isnegative(false)
{
	if (s.empty())
	{
		m_number.push_back(0);
		return;
	}

	if (s[0] == '-')
	{
		m_isnegative = true;
	}

	int string_length = s.size();
	int number_begin = m_isnegative;

	//While not empty consume digits in groups of m_base_size size starting from the end
	for (int index = string_length - 1; index >= number_begin; index -= m_base_size)
	{
		int digit = 0;
		int pow_base = 1;
		int digit_end = std::max(number_begin - 1, index - m_base_size);
		for (int digit_index = index; digit_index > digit_end; digit_index--, pow_base *= 10)
		{
			digit += (s[digit_index] - '0') * pow_base;
		}
		m_number.push_back(digit);
	}
}
#pragma endregion
#pragma region auxiliary arithmetic methods
BigInt& BigInt::Add(const BigInt& other, bool other_sign)
{
	//(-a) + (+b) = -((+a) - (+b))
	if (m_isnegative && !other_sign)
	{
		m_isnegative = false;
		Subtract(other, other_sign);
		m_isnegative = !m_isnegative;
	}

	//(+a) + (-b) = (+a) - (+b)
	if (!m_isnegative && other_sign)
	{
		Subtract(other, false);
	}

	int base = std::pow(10, m_base_size);
	size_t otherSize = other.base_digits();
	m_number.resize(std::max(this->base_digits(), otherSize) + 1);

	int k = 0, i = 0;
	for (; i < otherSize; i++)
	{
		int res = m_number[i] + other.m_number[i] + k;
		m_number[i] = res % base;
		k = res / base;
	}
	if (k)
		m_number[i] = k;
	else
		m_number.pop_back();
	return *this;
}

BigInt& BigInt::Add(BigInt&& other, bool other_sign)
{
	//(-a) + (+b) = -((+a) - (+b))
	if (m_isnegative && !other_sign)
	{
		m_isnegative = false;
		Subtract(other, other_sign);
		m_isnegative = !m_isnegative;
	}

	//(+a) + (-b) = (+a) - (+b)
	if (!m_isnegative && other_sign)
	{
		Subtract(other, false);
	}

	//sono nel caso (+-a) + (+-b)
	int base = std::pow(10, m_base_size);
	size_t otherSize = other.base_digits();

	if (otherSize > this->base_digits())
		this->m_number.swap(other.m_number);

	int k = 0, i = 0;
	for (; i < otherSize; i++)
	{
		int res = m_number[i] + other.m_number[i] + k;
		m_number[i] = res % base;
		k = res / base;
	}
	if (k)
		m_number.push_back(k);
	return *this;
}

BigInt& BigInt::Subtract(const BigInt& other, bool other_sign)
{
	//(+a) - (-b) = (+a) + (+b)
	if (!m_isnegative && other_sign)
		return Add(other, false);
	//(-a) - (+b) = (-a) + (-b)
	if (m_isnegative && !other_sign)
		return Add(other, true);
	//(-a) - (-b) = -((+a) - (+b))
	if (!m_isnegative && !other_sign)
	{
		m_isnegative = false;
		Subtract(other, false);
		m_isnegative = !m_isnegative;
		return *this;
	}
	const BigInt* a = this;
	const BigInt* b = &other;

	//sono nel caso (+a) - (+b)
	if (this->Compare(other) < 0)
	{
		if (base_digits() < other.base_digits())
			m_number.resize(other.base_digits());
		a = &other; b = this;
	}

	int base = powf(10, m_base_size);

	int k = 0;
	for (int i = 0; i < m_number.size(); i++)
	{
		int temp = (a->m_number[i] - b->m_number[i] + k);
		int mult = temp < 0;
		m_number[i] = base * mult + temp;
		k = temp / base;
	}
	if (m_number.back() == 0)
		m_number.pop_back();

	return *this;
}

BigInt& BigInt::Subtract(BigInt&& other, bool other_sign)
{
	//(+a) - (-b) = (+a) + (+b)
	if (!m_isnegative && other_sign)
		return Add(other, false);
	//(-a) - (+b) = (-a) + (-b)
	if (m_isnegative && !other_sign)
		return Add(other, true);
	//(-a) - (-b) = -((+a) - (+b))
	if (!m_isnegative && !other_sign)
	{
		m_isnegative = false;
		Subtract(other, false);
		m_isnegative = !m_isnegative;
		return *this;
	}
	const BigInt* a = this;
	const BigInt* b = &other;

	//sono nel caso (+a) - (+b)
	if (this->Compare(other) < 0)
	{
		m_number.swap(other.m_number);
	}

	int base = powf(10, m_base_size);

	int k = 0;
	for (int i = 0; i < m_number.size(); i++)
	{
		int temp = (a->m_number[i] - b->m_number[i] + k);
		int mult = temp < 0;
		m_number[i] = base * mult + temp;
		k = temp / base;
	}
	if (m_number.back() == 0)
		m_number.pop_back();

	return *this;
}

BigInt& BigInt::Multiply(const BigInt& other)
{
	size_t operandSize = std::max(m_number.size(), other.m_number.size());
	size_t resultSize = operandSize * 2;

	while (operandSize & (operandSize - 1)) {
		++operandSize;
	}

	unsigned long long* x = new unsigned long long[operandSize] {};
	unsigned long long* y = new unsigned long long[operandSize] {};
	unsigned long long* res = new unsigned long long[6 * operandSize]{};
	for (size_t i = 0; i < m_number.size(); i++)
		x[i] = (long long)m_number[i];
	for (size_t i = 0; i < other.m_number.size(); i++)
		y[i] = (long long)other.m_number[i];

	karatsuba_mul(res, x, y, operandSize);
	normalize(res, &resultSize, m_base_size);
	std::vector<long long> result{ res, res + resultSize };
	m_number.resize(resultSize);
	for (size_t i = 0; i < resultSize; i++)
		m_number[i] = result[i];
	m_isnegative = m_isnegative != other.m_isnegative;
	return *this;
}

BigInt& BigInt::Divide(const BigInt& other) 
{
	//TO DO: gestire divisione per 0!
	if (*this < other)
	{
		m_number.clear();
		m_number.push_back(0);
		m_isnegative = false;
		return *this;
	}

	if (*this == other)
	{
		m_number.clear();
		m_number.push_back(1);
		m_isnegative = m_isnegative != other.m_isnegative;
		return *this;
	}

	BigInt u(*this);
	BigInt v(other);

	std::size_t n = v.m_number.size();
	std::size_t m = u.m_number.size() - n;
	int b = pow(10, m_base_size);
	//Choose d!
	int d = 1;
	int last_v_digit = v.m_number[n - 1];
	while (last_v_digit < b / 2)
	{
		d *= 2;
		last_v_digit = (d * v).m_number[n - 1];
	}
	//int d = (b - 1) / other.m_number[n - 1];

	u = u * d;
	if (u.m_number.size() == m + n)
		u.m_number.push_back(0);
	v = v * d;
	BigInt quot = BigInt();

	int j = m;
	while (j >= 0)
	{
		int q = (u.m_number[j + n] * b + u.m_number[j + n - 1]) / v.m_number[n - 1];
		int r = (u.m_number[j + n] * b + u.m_number[j + n - 1]) % v.m_number[n - 1];
		while (q == b || (n >= 2 && q * v.m_number[n - 2] > b * r + u.m_number[j + n - 2]))
		{
			q -= 1;
			r += v.m_number[n - 1];
		}

		if (u.m_number.back() == 0)
			u.m_number.pop_back();

		u = u - (q * v).shift_left(j);
		quot = quot + BigInt(q).shift_left(j);
		j--;
	}
	*this = quot;
	return *this;
}

BigInt BigInt::SlowPower(const BigInt& b, long long unsigned e)
{
	//TO DO: controllare che l'esponente sia positivo!
	if (e == 0)
		return BigInt(1);
	if (e == 1)
		return b;
	BigInt res(b);
	while (e-- > 1)
	{
		res = res * b;
	}
	return res;
}

BigInt BigInt::Power(const BigInt& b, long long unsigned n)
{
	if (n == 0)
		return 1;
	BigInt y{ 1 };
	BigInt x(b);
	while (n > 1)
	{
		y = n % 2 ? x * y : y;
		x = x * x;
		n = n % 2 ? (n - 1) / 2 : n / 2;
	}
	return x * y;
}
#pragma endregion
#pragma region Karatsuba Mult
void BigInt::base_mul(unsigned long long* ret, unsigned long long* a, unsigned long long* b, size_t d)
{
	int i, j;

	for (i = 0; i < 2 * d; i++) ret[i] = 0;
	for (i = 0; i < d; i++) {
		for (j = 0; j < d; j++) ret[i + j] += a[i] * b[j];
	}
}

void BigInt::karatsuba_mul(unsigned long long* ret, unsigned long long* a, unsigned long long* b, size_t d)
{
	unsigned long long             i;
	unsigned long long* ar = &a[0]; // low-order half of a
	unsigned long long* al = &a[d / 2]; // high-order half of a
	unsigned long long* br = &b[0]; // low-order half of b
	unsigned long long* bl = &b[d / 2]; // high-order half of b
	unsigned long long* asum = &ret[d * 5]; // sum of a's halves
	unsigned long long* bsum = &ret[d * 5 + d / 2]; // sum of b's halves
	unsigned long long* x1 = &ret[d * 0]; // ar*br's location
	unsigned long long* x2 = &ret[d * 1]; // al*bl's location
	unsigned long long* x3 = &ret[d * 2]; // asum*bsum's location

	if (d <= 64)
	{
		base_mul(ret, a, b, d);
		return;
	}

	// compute asum and bsum
	for (i = 0; i < d / 2; i++) {
		asum[i] = al[i] + ar[i];
		bsum[i] = bl[i] + br[i];
	}

	//recursive calls
	karatsuba_mul(x1, ar, br, d / 2);
	karatsuba_mul(x2, al, bl, d / 2);
	karatsuba_mul(x3, asum, bsum, d / 2);

	//combine recursive steps
	for (i = 0; i < d; i++) x3[i] = x3[i] - x1[i] - x2[i];
	for (i = 0; i < d; i++) ret[i + d / 2] += x3[i];
}

void BigInt::normalize(unsigned long long* res, size_t* size, size_t base_size)
{
	int base = std::pow(10, base_size);
	for (auto i = 0; i < *size - 1; ++i) {
		res[i + 1] += res[i] / base;
		res[i] %= base;
	}
	while (*size > 1 && res[*size - 1] == 0)
		(*size)--;
}
#pragma endregion
#pragma region arithmetic operators
#pragma region operator+
BigInt& BigInt::operator+=(const BigInt& other){ return Add(other, other.m_isnegative); }

BigInt& BigInt::operator+=(BigInt&& other){ return Add(other, other.m_isnegative); }

BigInt BigInt::operator+(const BigInt& other) const { BigInt thisNum(*this); return thisNum += other; }

BigInt operator+(long long int val, const BigInt& c) { return BigInt(val) += c; }

BigInt BigInt::operator+(long long int val) const { BigInt thisNum(*this); return thisNum += BigInt(val); }
#pragma endregion
#pragma region operator-
BigInt & BigInt::operator-=(const BigInt& other) { return Subtract(other, other.m_isnegative); }

BigInt& BigInt::operator-=(BigInt&& other) { return Subtract(other, other.m_isnegative); }

BigInt BigInt::operator-(const BigInt& other) const { BigInt thisNum(*this); return thisNum -= other; }

BigInt operator-(long long int val, const BigInt& c) { return BigInt(val) -= c; }

BigInt BigInt::operator-(long long int val) const { return *this - BigInt(val); }
#pragma endregion
#pragma region operator*
BigInt BigInt::operator*=(const BigInt& other){ return Multiply(other); } 

BigInt BigInt::operator*(const BigInt& other) const{ BigInt thisNum(*this); return thisNum *= other; }

BigInt operator*(long long int val, const BigInt& c){ return BigInt(val) *= c; }

BigInt BigInt::operator*(long long int val) const { return *this * BigInt(val); }
#pragma endregion
#pragma region operator/
BigInt& BigInt::operator/=(const BigInt& other)
{
	return Divide(other);
}
BigInt BigInt::operator/(const BigInt& other) const
{
	BigInt thisNum(*this); return thisNum /= other;
}

BigInt operator/(long long int val, const BigInt& c)
{
	return BigInt(val) /= c;
}

BigInt BigInt::operator/(long long int val) const
{
	return *this / BigInt(val);
}
#pragma endregion
#pragma region operator%
BigInt& BigInt::operator%=(const BigInt& other)
{
	BigInt quot = *this / other;
	return this->Subtract(other * quot, quot.m_isnegative != other.m_isnegative);
}
BigInt BigInt::operator%(const BigInt& other) const
{
	BigInt thisNum(*this); return thisNum %= other;
}

BigInt operator%(long long int val, const BigInt& c)
{
	return BigInt(val) /= c;
}

BigInt BigInt::operator%(long long int val) const
{
	return *this % BigInt(val);
}
#pragma endregion
#pragma region boolean operators
//la funzione ritorna:
//  0 - se i due numeri sono uguali
// -1 - se *this � minore di other
// +1 - se *this � > di other
int BigInt::Compare(const BigInt& other) const //0 this == a || -1 this < a || 1 this > a
{
	if (!m_isnegative && other.m_isnegative) return 1;
	if (m_isnegative && !other.m_isnegative) return -1;

	int check = 1;
	if (m_isnegative && other.m_isnegative) check = -1;

	if (base_digits() < other.base_digits()) return -1 * check;
	if (base_digits() > other.base_digits()) return check;
	for (int i = base_digits() - 1; i >= 0; i--) {
		if (m_number[i] < other.m_number[i]) return -1 * check;
		if (m_number[i] > other.m_number[i]) return check;
	}
	return 0;
}

BigInt operator>(long long int val, const BigInt& c)
{
	return BigInt(val) > c;
}

BigInt operator<(long long int val, const BigInt& c)
{
	return BigInt(val) < c;
}

BigInt operator==(long long int val, const BigInt& c)
{
	return BigInt(val) == c;
}

BigInt operator>=(long long int val, const BigInt& c)
{
	return BigInt(val) >= c;
}

BigInt operator<=(long long int val, const BigInt& c)
{
	return BigInt(val) <= c;
}

bool BigInt::operator>(const BigInt& other) const
{
	return Compare(other) == 1;
}

bool BigInt::operator<(const BigInt& other) const
{
	//return !(*this > other) && !(*this == other);
	return Compare(other) == -1;
}

bool BigInt::operator==(const BigInt& other) const
{
	return Compare(other) == 0;
}
bool BigInt::operator>=(const BigInt& other) const
{
	return Compare(other) >= 0;
}

bool BigInt::operator<=(const BigInt& other) const
{
	return Compare(other) <= 0;
}

bool BigInt::operator>(long long int val) const
{
	return *this > BigInt(val);
}

bool BigInt::operator<(long long int val) const
{
	return *this < BigInt(val);
}

bool BigInt::operator==(long long int val) const
{
	return *this == BigInt(val);
}

bool BigInt::operator>=(long long int val) const
{
	return *this >= BigInt(val);
}

bool BigInt::operator<=(long long int val) const
{
	return *this <= BigInt(val);
}
#pragma endregion
#pragma region bit operators
BigInt& BigInt::shift_left(size_t n)
{
	if (*this == 0)
		return *this;
	m_number.insert(m_number.begin(), n, 0);
	/*m_number.resize(m_number.size() + n, 0);
	size_t newSize = m_number.size();
	for (size_t i = newSize; i > 0; i--)
	{
		m_number[i - 1] = m_number[i - 1 - n];
		m_number[i - 1 - n] = 0;
	}	
	BigInt res;
	res.m_isnegative = m_isnegative;
	std::vector<int> res_num{};
	res_num.reserve(n + m_number.size());
	std::vector<int> zeroes(n, 0);
	res_num.insert(res_num.end(), zeroes.begin(), zeroes.end());
	res_num.insert(res_num.end(), m_number.begin(), m_number.end());
	res.m_number = res_num;
	return res;*/
	return *this;
}

BigInt& BigInt::shift_right(size_t n)
{
	if (*this == 0)
		return *this;
	m_number.erase(m_number.begin(), m_number.begin() + n + 1);
	return *this;
}

BigInt BigInt::operator<<(int v)
{
	std::string num = this->to_string();
	while (v > 0)
	{
		num += '0';
		v--;
	}
	return BigInt(num);
}

BigInt BigInt::operator>>(int v)
{
	std::size_t num_digits = digits();
	if (v >= num_digits)
		return BigInt("");
	return BigInt(this->to_string().substr(0, num_digits - v - 1));
}
#pragma endregion
#pragma region other operators
std::ostream& operator<<(std::ostream& out, const BigInt& c)
{
	std::string s;
	if (c.m_isnegative)
		s.push_back('-');

	s.append(std::to_string(c.m_number.back()));

	size_t size = c.m_number.size();
	int base = std::pow(10, c.m_base_size);

	for (int i = size - 2; i >= 0; --i)
	{
		int digit_base = base / 10;
		int digit = c.m_number[i];
		while (digit_base > 0)
		{
			s.push_back('0' + digit / digit_base);
			digit %= digit_base;
			digit_base /= 10;
		}
	}
	out << s;
	return out;
}

std::string BigInt::to_string() const
{
	std::stringstream ss;
	ss << *this;
	std::string s;
	ss >> s;
	return s;
}

std::size_t BigInt::digits() const
{
	return m_number.size() * m_base_size;
}

std::size_t BigInt::base_digits() const
{
	return m_number.size();
}

BigInt& BigInt::operator++()
{
	int base = pow(10, m_base_size);
	if (m_number[0] == base - 1)
		*this = *this + 1;
	else
		m_number[0]++;
	return *this;
}
BigInt BigInt::operator++(int)
{
	BigInt res(*this);
	++* this;
	return res;
}
BigInt& BigInt::operator--()
{
	int base = pow(10, m_base_size);
	if (m_number[0] == 0)
		*this = *this - 1;
	else
		m_number[0]--;
	return *this;
}
BigInt BigInt::operator--(int)
{
	BigInt res(*this);
	--* this;
	return res;
}
bool BigInt::odd() const
{
	return m_number[0] % 2;
}

bool BigInt::even() const
{
	return !odd();
}
BigInt BigInt::operator-() const
{
	BigInt res = *this;
	res.m_isnegative = !res.m_isnegative;
	return res;
}
#pragma endregion







