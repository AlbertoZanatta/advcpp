#pragma once
#include <vector>
#include <array>
#include <deque>
#include <cstdint>
#include <string>
#include <ostream>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>
//?? Change it to a class template with type traits (es. base = 10^(std::numeric_limits<char>::digit10)
//   See: https://stackoverflow.com/questions/14294267/class-template-for-numeric-types
class BigInt
{
private:
	std::vector<int> m_number;
	uint8_t m_base_size = 9; //which means 10^9
	bool m_isnegative;
public:
	BigInt();
	BigInt(std::vector<int>, bool = false);
	BigInt(std::vector<long long>, bool = false);
	BigInt(std::string);
	BigInt(long long int);
	BigInt(const char* num);
	std::string to_string() const;
	
	//Addition
	BigInt& operator+=(BigInt&& other);
	BigInt& operator+=(const BigInt& other);
	BigInt operator+(const BigInt& other) const;
	BigInt operator+(long long int) const;
	friend BigInt operator+(long long int, const BigInt&);
	BigInt& Add(const BigInt& other, bool other_sign);
	BigInt& Add(BigInt&& other, bool other_sign);

	BigInt& operator-=(const BigInt& other);
	BigInt& operator-=(BigInt&& other);
	BigInt operator-(const BigInt& other) const;
	BigInt operator-(long long int) const;
	friend BigInt operator-(long long int, const BigInt&);

	BigInt& Subtract(const BigInt& other, bool other_sign);
	BigInt& Subtract(BigInt&& other, bool other_sign);
	
	BigInt operator*=(const BigInt& other);
	BigInt operator*(long long int) const;
	BigInt operator*(const BigInt& other) const;
	friend BigInt operator*(long long int, const BigInt&);
	
	BigInt& operator/=(const BigInt& other);
	BigInt operator/(long long int) const;
	BigInt operator/(const BigInt& other) const;
	friend BigInt operator/(long long int, const BigInt&);
	
	BigInt& operator%=(const BigInt& other);
	BigInt operator%(long long int) const;
	BigInt operator%(const BigInt&) const;
	friend BigInt operator%(long long int, const BigInt&);


	BigInt& shift_left(size_t n = 1);
	BigInt& shift_right(size_t n = 1);
	std::size_t digits() const;
	std::size_t base_digits() const;

	bool operator>(const BigInt& other) const;
	bool operator>(long long int) const;
	friend BigInt operator>(long long int, const BigInt&);

	bool operator<(const BigInt& other) const;
	bool operator<(long long int) const;
	friend BigInt operator<(long long int, const BigInt&);

	bool operator==(const BigInt& other) const;
	bool operator==(long long int) const;
	friend BigInt operator==(long long int, const BigInt&);

	bool operator>=(const BigInt& other) const;
	bool operator>=(long long int) const;
	friend BigInt operator>=(long long int, const BigInt&);

	bool operator<=(const BigInt& other) const;
	bool operator<=(long long int) const;
	friend BigInt operator<=(long long int, const BigInt&);

	friend std::ostream& operator<<(std::ostream& out, const BigInt& c);
	BigInt& operator++();
	BigInt operator++(int);
	BigInt& operator--();
	BigInt operator--(int);
	BigInt operator<<(int);
	BigInt operator>>(int);

	static BigInt SlowPower(const BigInt&, long long unsigned);
	static BigInt Power(const BigInt&, long long unsigned);
	bool odd() const;
	bool even() const;
	static void normalize(std::vector<long long>& res, size_t base_size);
	static void to_power2_length(std::vector<long long>& v, size_t len);
	static std::vector<long long> karatsuba_mul(const std::vector<long long>& x, const std::vector<long long>& y);
	static std::vector<long long> base_mul(const std::vector<long long>& x, const std::vector<long long>& y);
	BigInt fast_mul(const BigInt& other) const;
	BigInt& Multiply(const BigInt& other);
	BigInt& Divide(const BigInt& other);
	static void karatsuba_mul(unsigned long long* res, unsigned long long* x, unsigned long long* y, size_t size);
	static void base_mul(unsigned long long* res, unsigned long long* x, unsigned long long* y, size_t size);
	static void normalize(unsigned long long* res, size_t* size, size_t base_size);

	BigInt operator-() const;
	int Compare(const BigInt& a) const;
};